-   [Parallelization in R (on Metworx)](#parallelization-in-r-on-metworx)
    -   [Setup](#setup)
-   [Examples of trial simulation](#examples-of-trial-simulation)
    -   [lapply](#lapply)
    -   [mclapply: parallelize on the master node](#mclapply-parallelize-on-the-master-node)
    -   [qapply: distribute work to the cluster](#qapply-distribute-work-to-the-cluster)
-   [Object visibility](#object-visibility)
-   [Monitoring and interacting with the grid](#monitoring-and-interacting-with-the-grid)
    -   [Status](#status)
    -   [Delete](#delete)
-   [Debugging](#debugging)
    -   [Logging](#logging)
    -   [Replicating the compute environment](#replicating-the-compute-environment)

Parallelization in R (on Metworx)
=================================

Here we demonstrate how to parallelize work in R using **qapply** and the **parallel** R package. One of the most useful features of (www.metworx.com)\[Metworx\] is the autoscaling functionality and the ability to push work onto compute nodes, rather than consuming master node resources. Pushing work into the compute nodes must happen via [SGE](https://arc.liv.ac.uk/SGE/) on Metworx in order for autoscaling to happen, but do note that it's possible to use other packages to accomplish the same task (e.g., [foreach and doParallel](https://cran.r-project.org/web/packages/foreach/vignettes/foreach.pdf).

Setup
-----

The easiest way to do this is to use `devtools::install_bitbucket`.

``` r
install.packages("devtools")
library(devtools)
install_bitbucket("metrumrg/qapply",subdir="working/qapply")

library(parallel)  
library(qapply)    # Use qapply to push jobs out to compute nodes
```

This will install the HEAD commit; alternatively, you can specify a specific release of **qapply** to install like this:

``` r
install_bitbucket("metrumrg/qapply",subdir="working/qapply",ref="1.38")
```

    ## Downloading bitbucket repo metrumrg/qapply@1.38

    ## Installing qapply

    ## '/usr/lib/R/bin/R' --no-site-file --no-environ --no-save --no-restore  \
    ##   --quiet CMD INSTALL  \
    ##   '/tmp/RtmpGx9OV6/devtools32f838716596/metrumrg-qapply-cc7899e8431d/working/qapply'  \
    ##   --library='/data/qapply/lib' --install-tests

    ## 

    ## Reloading installed qapply

Examples of trial simulation
============================

Let's define a nonsense function that simulates a trial, returning a data.frame of times, treatments, and endpoint values:

``` r
trialSim <- function(npat){
  ## Here would be your trial simulation code
  progression <- 1
  baseline <- 0
  deff <- .5
  
  out <- expand.grid(
    Patient=1:npat,
    Time=c(0,1),
    Arm=c(0,1)
  )
  
  out$EP <- exp( rnorm(1:nrow(out), 
                       mean= baseline + 
                         progression * (deff*out$Arm)  * out$Time ,
                       sd=1
                       )
  )
  
  dplyr::group_by(out, Time, Arm) %>% dplyr::summarize(Mean=mean(EP))
}

trialSim(60)
```

    ## Source: local data frame [4 x 3]
    ## Groups: Time [?]
    ## 
    ##    Time   Arm     Mean
    ##   <dbl> <dbl>    <dbl>
    ## 1     0     0 1.641032
    ## 2     0     1 1.614276
    ## 3     1     0 2.241277
    ## 4     1     1 3.200267

lapply
------

To run this serially, we could use `lapply` to apply the `trialSim` function to 60 patients 10k times. If you have `htop` available via the command line, take a look at what happens when you do this; you'll see only core being used as the jobs are not being threaded and are being executed one after another.

``` r
system.time(serial <- lapply(rep(60, 10000), trialSim) )
```

    ##    user  system elapsed 
    ##  13.425   0.020  13.449

``` r
class(serial)
```

    ## [1] "list"

``` r
serial[[1]]
```

    ## Source: local data frame [4 x 3]
    ## Groups: Time [?]
    ## 
    ##    Time   Arm     Mean
    ##   <dbl> <dbl>    <dbl>
    ## 1     0     0 1.881483
    ## 2     0     1 2.273665
    ## 3     1     0 1.976138
    ## 4     1     1 2.508062

mclapply: parallelize on the master node
----------------------------------------

We can get an immediate speedup by just using what's available to us on the master node. Let's say we have 8 cores available, then we can perform the same task using `mclapply` which will execute an `lapply` while distributing the jobs across the master node. Again, if `htop` is available to you, take a look at what happens on the cores.

``` r
system.time(parallel.fork <- parallel::mclapply(rep(60, 10000), trialSim, mc.cores = 8) )
```

    ##    user  system elapsed 
    ##  19.472   0.429   3.143

``` r
class(parallel.fork)
```

    ## [1] "list"

``` r
parallel.fork[[1]]
```

    ## Source: local data frame [4 x 3]
    ## Groups: Time [?]
    ## 
    ##    Time   Arm     Mean
    ##   <dbl> <dbl>    <dbl>
    ## 1     0     0 2.132985
    ## 2     0     1 1.713597
    ## 3     1     0 2.118658
    ## 4     1     1 2.662949

Be careful with this. You are consuming resources on the master node, and you should expect Rstudio connectivity to suffer considerably if the server is crunching numbers for you rather than serving Rstudio.

qapply: distribute work to the cluster
--------------------------------------

Distributing the work to the cluster will free up the master node to perform important interface services and allow you to tap into the autoscaling functionality of Metworx. It's important to note, however, that in many situations this won't be faster than `mclapply`. It's not until you get into situations where the function (`trialSims` here) takes a while to execute and you have many more jobs to complete than cores available on the master node that a large speedup is obvious for **qapply**. To distribute the jobs across the grid with qapply, a simple example would be:

``` r
system.time(parallel.sge <- qapply(rep(60, 10000), trialSim, tag="trialSim", nCores=16) )
```

    ##    user  system elapsed 
    ##   0.486   0.335  23.840

``` r
class(parallel.sge)
```

    ## [1] "list"

``` r
parallel.sge[[1]]
```

    ## Source: local data frame [4 x 3]
    ## Groups: Time [?]
    ## 
    ##    Time   Arm     Mean
    ##   <dbl> <dbl>    <dbl>
    ## 1     0     0 1.697614
    ## 2     0     1 1.297545
    ## 3     1     0 1.383834
    ## 4     1     1 2.533630

The `nCores` argument is telling **qapply** how many chunks of jobs in which to break up the application of the function to the first argument. If these jobs take a while (hours), and I've told Metworx I want to max out at 10 nodes of 32 cores each, then I would likely set `nCores=10*32`. If each execution of the function (`trialSims` here) is quick, then it makes more sense to just select the number of cores on a single compute node to cut down on the time required to manage I/O, e.g., `nCores=32`.

Object visibility
=================

If you look at the R help for **qapply** (`?qapply`), you'll see a few arguments that are related to the scope of objects your function might need to execute.

-   `global=T`: tells **qapply** to make your entire R environment at the time of calling **qapply** available to the compute nodes
-   `global=F`: means you need to use `commonData` to make sure all objects needed by the function are available. You will also want to use `fArgs` to provide a list of the objects to be passed as arguments into the function.

For example, let's redefine `trialSim` as such:

``` r
# Define an Arm lookup **that exists only in the global environment**:
ArmNames <- data.frame(from=c(0,1), to=c("Placebo","Drug"))

# Use arguments to the function
trialSim <- function(npat, progression=1, baseline=0, deff=.5){
  
  out <- expand.grid(
    Patient=1:npat,
    Time=c(0,1),
    Arm=c(0,1)
  )
  
  out$EP <- exp( rnorm(1:nrow(out), 
                       mean= baseline + 
                         progression * (deff*out$Arm)  * out$Time ,
                       sd=1
                       )
  )
  
  # Refer to ArmNames, which isn't actually contained in the environment of this function
  out$Arm <- ArmNames$to[match(out$Arm,ArmNames$from)]
  
  dplyr::group_by(out, Time, Arm) %>% dplyr::summarize(Mean=mean(EP))
}

trialSim(30, progression=2, baseline=0, deff=.9)
```

    ## Source: local data frame [4 x 3]
    ## Groups: Time [?]
    ## 
    ##    Time     Arm     Mean
    ##   <dbl>  <fctr>    <dbl>
    ## 1     0    Drug 1.626317
    ## 2     0 Placebo 2.142145
    ## 3     1    Drug 9.187928
    ## 4     1 Placebo 1.565450

Now you would use one of the following methods to run the simulations via **qapply**:

``` r
# Pass the global environment
parallel.sge.global <- qapply(rep(60, 10000), trialSim, tag="trialSim", nCores=16, 
                              global=T, fargs = list(progression=2, baseline=0, deff=.9))

# Pass only the specific arguments we need
parallel.sge.explicit <- qapply(rep(60, 10000), trialSim, tag="trialSim", nCores=16, 
                              fargs = list(progression=2, baseline=0, deff=.9),
                              commonData = list(ArmNames=ArmNames)
                              )
```

Monitoring and interacting with the grid
========================================

The **qapply** R package contains several functions to assist in interacting with the grid.

Status
------

``` r
?qstat
qstat("-f")
qstat("-u danp")
```

Delete
------

``` r
?qdel
qdel("-u danp")
qdel(1:100)
```

Debugging
=========

It's inevitable that you'll run across a problem at some point, most likely in the form of receiving a `NULL` return from `qapply` or `qinternalize`. Debugging is a bit more complicated now as you're pushing jobs out to remote nodes. The complexity of replicating the calling environment on the compute nodes is not a trivial matter, but you can examine the output from the nodes, the error logs from the nodes, and as a last resort recreate the steps **qapply** is taking for the most stubborn issues.

Logging
-------

**qapply** creates three subdirectories of your working directory (`workDir`) to record help you debug and store the output from the runs:

-   `<workDir>/out/<tag>`: R binary output from each of the compute nodes. This output can be aggregated and returned into a master environment by using the `qinternalize` command.
-   `<workDir>/err/<tag>`: `stderr` from each of the compute nodes. These are typically the most useful when you run into issues.
-   `<workDir>/log/<tag>`: `stdout` from each of the compute nodes.

**Note that you need to have `clearWd=FALSE` or `internalize=FALSE` to see files.**

Replicating the compute environment
-----------------------------------

If the logs in `err` and `out` don't lead you to a solution, try replicating the compute environment. In a new R session, load the file *qcontrol.R*. This file is what is executed on the compute nodes, so running through it in a separate R session will replicate what is happening on the grid compute nodes.

In the new session, define the following variables:

-   `tag`: The same tag you used when requesting `qapply`
-   `begin=1`
-   `end=2`

Skip down and run lines 24-40 of `qcontrol.R` and load the libraries and objects passed via `commonData`. You can skip the seeding process, and try running the job in lines 49-59. Running the debugger on `FUN` will let you use R's debugging mechanism to step through the problematic code.
