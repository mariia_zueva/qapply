#' @name qapply
#'
#' @title Parallel lapply using grid engine
#'
#' @description  The qapply function will attempt to replicate the behavior of lapply using grid
#'  engine, applying a function to each element of the first argument (\code{X})
#'  and returning a list.
#'
#' @usage  qapply(X, FUN, fargs, global=FALSE, all.names=FALSE, commonData = NULL, workDir = getwd(), nCores =
#'           as.numeric(.getResources()), nPerPacket = NULL, tag =
#'           deparse(substitute(X)), internalize=TRUE, clearWd=TRUE, parSeed=NULL)
#'
#' @param X An array or list of values to be evaluated by \code{FUN}
#' @param FUN A function to be applied to \code{X}
#' @param fargs A list of (named) arguments for \code{FUN}
#' @param global Pass the entire global environment (TRUE) to slave processes?  FALSE
#'    by default as it's easy to flood system resources with large global
#'    environments.
#' @param all.names  When using global=TRUE, set all.names=TRUE if there are hidden
#'    objects (i.e., objects beginning with ".") to be passed.
#' @param commonData A list of (named) objects that will be visible to each child
#'    process.  Providing a non NULL value of this argument overrides global=TRUE.
#' @param workDir Where the worker processes will be spawned, and where the "out",
#'    "err", and "log" temporary files will be created
#' @param nPerPacket A vector of integers whose sum is \code{length(X)}.  Packet \code{i}
#'    contains \code{nPerPacket[i]} elements of \code{X}.
#' @param nCores The number of computational cores to be used for the \code{length(X)} individual
#'    jobs.  When \code{nPerPacket} is unspecified, \code{qapply} will
#'    build \code{nCores} packets of jobs to reduce overhead tied to configuring the worker processes.
#'    The default value of \code{nCores} is to take the number available to the grid at the
#'    time of \code{qapply}'s execution.  
#' @param tag A label for the job and temporary files related to the job.  The
#'    default is to take the name of \code{X}.
#' @param internalize  When TRUE, \code{qapply} will return a list containing the output of
#'    \code{FUN} for each element of \code{X}.  When FALSE, \code{qapply}
#'    acts simply to facilitate submission of jobs to the queue.  See \code{qsub}.
#' @param parSeed A vector of six integers which is passed to the \code{rlecuyer}
#'    package for generating parallel random number streams.  If left blank,
#'    the default is \code{rep(1111, 6)}.  See \code{qseed}
#' @param lib Library path to hand off to worker processes
#' @param q.q Name of queue to submit to ("all" by default)
#' @param grid logical, run on grid (TRUE) or run locally?
#' @details  When \code{grid=TRUE}, the \code{qapply} function utilizes the
#'  wrapper \code{\link{qsub}} to
#'  perform an lapply.  This is done by instructing worker processes to
#'  evaluate the function on subsets of \code{X} called packets.  Each
#'  packet contains a number of jobs, where each job is a single
#'  application of \code{FUN} to an element of \code{X}. When \code{grid=FALSE}, 
#'  \code{parallel::mclapply} is attempted on unix systems else a bare \code{lapply}
#'  is used.
#'  
#'  The finest granularity is achieved by setting
#'  \code{nPerPacket=rep(1, length(X))}.  While useful on expandable
#'  clouds (i.e., nCores is not fixed), this behavior explicitly implies
#'  that \code{length(X)} R processes will be loaded and configured.  The
#'  time it takes to set up each of these processes depends upon the size
#'  of the \code{commonData} and \code{fargs} objects as these must be loaded in the
#'  new environment.
#'  
#'  If the user does not specify commonData and uses global=TRUE, all
#'  objects in the global environment for the calling R session will be
#'  made available to slave processes.  Large global environments will
#'  likely lead to memory limitations, in which case the user should pass
#'  necessary (and only necessary) data through the commonData argument.
#'  
#'  The library path of the parent environment and all loaded libraries will be preserved in child processes.  At this time, however, the same can not be said for "options". 
#'  
#'  Upon execution \code{qapply} assembles "packets" of jobs to be
#'  evaluated on a single core, writes out the
#'  common environment as an R object, and creates the "out", "log", and
#'  "err" subdirectories of \code{workDir}.  The "log" and "err"
#'  subdirectories can be helpful in debugging and will remain intact
#'  unless clearWd is set to TRUE.  By default (with \code{internalize=TRUE}) the R process will wait for
#'  all jobs to complete (see \code{\link{qwait}}).  When complete, the
#'  temporary output files in \code{file.path(workDir, "out")} will be
#'  aggregated and returned as a list using \code{\link{qinternalize}}.  
#'  
#'
#' @return When internalize=TRUE, A list resulting from the application of \code{FUN} to \code{X}.
#' @author Dan Polhamus <danp@metrumrg.com>
#' @seealso \code{\link{qsub}} \code{\link{rlecuyer}} \code{\link{qinternalize}} \code{\link{qstat}} \code{\link{qseed}} \code{\link{qwait}}
#' @examples 
#'  whatLetter <- function(letter){
#'    number <- which(letter == letters)
#'    ## Prefix is some data defined in the parent environment
#'    paste(prefix, letter, "is letter number", number)
#'  }
#'  
#'  prefix <- "This letter:"
#'  system.time(parallel <- qapply(letters, whatLetter, commonData=list(
#'    prefix=prefix )))
#'  
#'  ## This is much faster without using grid engine, as will always be the
#'  ## case when the evaluation of FUN is fast.
#'  
#'  
#'  ## -----------------------------------------------------------------------------
#'  
#'  invMatrix <- 
#'    
#'    qapply(rep(500, 1000), function(nr){
#'      solve( matrix(rnorm(nr^2), nrow=nr, ncol=nr) )
#'    }, internalize=FALSE, tag="invertMatrix")
#'  
#'  parallel <- qinternalize("out/invertMatrix")
#'  qclearWd()
#' @return When \code{internalize=TRUE}, a list the same length as \code{X}
#' @keywords parallel, HPC, SGE
#' @export

qapply <- function(X, FUN, fargs=NULL, commonData=NULL, workDir=getwd(),
                   nCores=NULL, global=FALSE,
                   all.names=FALSE, nPerPacket=NULL,
                   tag=deparse(substitute(X)), internalize=TRUE,
                   clearWd=TRUE, parSeed=NULL, lib=NULL, q.q=NULL,
                   q.pe=NULL, grid=TRUE)
{
  FUN <- match.fun(FUN)
  results <- NULL
  if (!is.vector(X) || is.object(X))
    X <- as.list(X)
  if (is.null(nCores)) nCores <- as.numeric(.getResources())
  if (!is.numeric(nCores) | (length(nCores)==0)) nCores=16
  if (is.null(nPerPacket)) nPerPacket=qapply:::.nPerPacket(nr=length(X), nc=nCores)
  nPerPacket <- nPerPacket[ !is.na(nPerPacket) & !is.null(nPerPacket) & (nPerPacket!=0) ]
  if (sum(nPerPacket) != length(X)){
    print("Specify nPerPacket as an array whose sum matches nrow(X)")
    return(NULL)
  }

  ## Prevent errors from sloppy tag usage:
  tag <- gsub("[/:*?\"'<>|]", "_", tag)
  if(length(list.dirs(file.path(workDir, "out", tag))) > 0) qclearWd(workDir, tag)
  oldDir <- getwd()
  on.exit(setwd(oldDir))
  setwd(workDir)
  
  if(!grid){
    
    argList <- list(X=X, FUN=FUN, fargs=fargs, commonData=commonData, workDir=workDir,
                    nCores=nCores, global=global,
                    all.names=all.names, nPerPacket=nPerPacket,
                    tag=tag, internalize=internalize,
                    parSeed=parSeed)
    results <- do.call(as_qapply, argList)

  }else{
      
    if(is.null(commonData) & global){ commonData=as.list(.GlobalEnv, all.names) }
    packages <- (.packages())
    if("shiny" %in% packages) packages <- packages[ -grep("shiny",packages) ] # Don't load the shiny package(s)
    commonEnv <- list( commonData=commonData, X=X, FUN=FUN, fargs=eval(fargs), packages=packages )
    if(is.null(lib)){ libPaths = .libPaths() }else{ libPaths=lib }
    save( libPaths, file=paste(tag, "libs.Rdata", sep="-"))
    save( commonEnv, file=paste(tag, "common.Rdata", sep="-"))
    ## On NFS shares, sometimes these take a while to propogate...  Pass the md5 sum to qsub
    library(tools)
    md5lib <- md5sum(paste(tag,"libs.Rdata",sep="-"))
    md5common <- md5sum(paste(tag,"common.Rdata",sep="-"))
    
    unlink("qcontrol.R")  
    funScript <- system.file("qcontrol.R", package="qapply", mustWork=TRUE)
    file.copy(funScript, "qcontrol.R")
    if( is.null(parSeed) ){ parSeed <- rep(1111, 6) }
    if( length(parSeed)!=6 ){ stop("parSeed must be six integers")  }
    if(suppressWarnings( length(system("qconf -sel",intern=T,ignore.stderr = T))==0 )){
      warning("No execution hosts immediately available.  This is not cause for alarm in autoscaling environments.",
              immediate.=TRUE)
    }
    for(i in 1:length(nPerPacket)){
      rowBegin <- ifelse( i==1, 1, sum(nPerPacket[1:(i-1)])+1 )
      rowEnd <- sum(nPerPacket[1:i])
      qsub("R --no-save --no-restore", 
           tag=tag, 
           rowBegin=rowBegin, 
           rowEnd=rowEnd,
           q.q=q.q,
           q.pe=q.pe,
           cmdArgs= c("--args", 
                      tag, 
                      rowBegin, 
                      rowEnd, 
                      i, 
                      length(nPerPacket), 
                      parSeed, 
                      md5lib, 
                      md5common, 
                      "< qcontrol.R"))
    }
    if(internalize){
      qwait(tag)
      results <- qinternalize(file.path("out", tag))
    }
  }

  if(length(results) != length(X)){warning("Some jobs did not complete, length(x)!=length(results).  Check error logs with clearWd=F for more information") }      
  if(clearWd){
    qclearWd(workDir,tag)
  }
  

  return(results)
}
