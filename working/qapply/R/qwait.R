#' @name qwait
#' @title Wait for a named job in the queue to finish
#' @description 
#'  Passing in the job name (or tag), \code{qwait} ties up R until all jobs
#'  matching "tag" are finished before returning control to R.
#' @usage qwait(tag)
qwait <- function(tag){
  continue <- TRUE
  while( continue ){
    system("sleep 1")
    jobs <- qstat()
    if(length(jobs)>0){
      jobs <- matrix(str_split(str_trim(jobs[-c(1,2)])," ",simplify=TRUE)[,c(1,3)], ncol=2)
      continue <- tag %in% jobs[,2]
    } else {
      continue <- FALSE
    }
  }
}