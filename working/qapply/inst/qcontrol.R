## --------------------------------------------------
## Paths
## --------------------------------------------------

## Job arguments:
## 1) Number of cores to parallelize over
argin <- commandArgs()  ## Grab the job arguments
arg <- argin[(which(argin=="--args")+1):length(argin)]
tag <- arg[1]
begin <- as.numeric(arg[2])
end <- as.numeric(arg[3])
jobNum <- as.numeric(arg[4])
jobTot <- as.numeric(arg[5])
parSeed <- ifelse(is.na(as.numeric(arg[6:11])), FALSE, as.numeric(arg[6:11]))
md5lib <- arg[12]
md5common <- arg[13]

print("--------------------")
print(paste("Job:",tag))
print(paste("Begin:", begin))
print(paste("End:", end))
print("--------------------")

library(tools)
if(md5common != md5sum(paste(tag,"common.Rdata",sep="-"))){
  Sys.sleep(5)
  if(md5common != md5sum(paste(tag,"common.Rdata",sep="-"))) stop("Environment object md5's don't match.")
} 
if(md5lib != md5sum(paste(tag,"libs.Rdata",sep="-"))){
  Sys.sleep(5)
  if(md5lib != md5sum(paste(tag,"libs.Rdata",sep="-"))) stop("Library object md5's don't match.")
} 


load(paste(tag,"libs.Rdata",sep="-"))
.libPaths(libPaths)
load(paste(tag,"common.Rdata", sep="-"))
for(pkg in commonEnv[["packages"]]){ require(pkg, character.only=TRUE)}

attach(commonEnv); attach(commonData)


if(is.numeric(parSeed)){
  qapply:::qseed(jobNum=jobNum, jobTot=jobTot, parSeed=parSeed)
}else{
  print("Not using qapply seed scheme \n")
}

outVec <- vector(length=end-begin, mode="list")
counter <- 0
for(i in begin:end){
  counter <- counter + 1
  if( is.null(fargs) ){
    outVec[[counter]] <- FUN(X[[i]])
  }else{ fargs.tmp <- as.list(c(X[i],fargs))
         names(fargs.tmp)[1] <- names(formals(FUN))[1]
         outVec[[counter]] <- do.call(FUN, fargs.tmp)
}
}
dir.create(file.path(getwd(), "out", tag), recursive=TRUE)

save( outVec, file=file.path(getwd(), "out", tag, paste('outVec-', begin, '-', end, '.Robj', sep='') ))
